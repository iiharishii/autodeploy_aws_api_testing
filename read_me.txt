AWS automation python scripts list:
1. AWS Webconsole Connection
2. AWS EC2 Instance Describe
3. AWS EC2 Instance ID reason by listing
4. AWS EC2 Instance Monitoring Status
5. AWS EC2 Instance Stop/Start
6. AWS EC2 Instance Reboot
7. AWS Describe Key Pair
8. AWS Create Key Pair
9. AWS Delete Key Pair
10. Describe Regions and Availability Zone
11. Describe security groups
12. Create a security group and rules
13. Delete a security group

