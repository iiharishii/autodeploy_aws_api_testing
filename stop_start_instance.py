import sys
import boto3
from botocore.exceptions import ClientError

instance_id = sys.argv[0]
action = sys.argv[0].upper()
aws_ec2_con=boto3.session.Session(profile_name='amit')
ec2 = aws_ec2_con.client('ec2', 'us-east-2')


if action == 'ON':
    # Do a dryrun first to verify permissions
    try:
        ec2.start_instances(InstanceIds=['i-0e626ec6d304b305f'], DryRun=True)
    except ClientError as e:
        if 'DryRunOperation' not in str(e):
            raise

    # Dry run succeeded, run start_instances without dryrun
    try:
        response = ec2.start_instances(InstanceIds=['i-0e626ec6d304b305f'], DryRun=False)
        print(response)
    except ClientError as e:
        print(e)
else:
    # Do a dryrun first to verify permissions
    try:
        ec2.stop_instances(InstanceIds=['i-0e626ec6d304b305f'], DryRun=True)
    except ClientError as e:
        if 'DryRunOperation' not in str(e):
            raise

    # Dry run succeeded, call stop_instances without dryrun
    try:
        response = ec2.stop_instances(InstanceIds=['i-0e626ec6d304b305f'], DryRun=False)
        print(response)
    except ClientError as e:
        print(e)