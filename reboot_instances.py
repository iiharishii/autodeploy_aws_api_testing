import boto3
from botocore.exceptions import ClientError

aws_mag_con = boto3.session.Session(profile_name='amit')
ec2_con_re = aws_mag_con.resource(service_name='ec2', region_name='us-east-2')

ec2 = aws_mag_con.client('ec2','us-east-2')

insst_id = ''
for each_ins in ec2_con_re.instances.all():
    insst_id=each_ins.id
print(insst_id)

try:
    ec2.reboot_instances(InstanceIds=[insst_id], DryRun=True)
except ClientError as e:
    if 'DryRunOperation' not in str(e):
        print("You don't have permission to reboot instances.")
        raise

try:
    response = ec2.reboot_instances(InstanceIds=[insst_id], DryRun=False)
    print('Success', response)
except ClientError as e:
    print('Error', e)