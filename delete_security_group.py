import boto3
from botocore.exceptions import ClientError

aws_man_con = boto3.session.Session(profile_name='amit')
aws_ec2_con = aws_man_con.client('ec2','us-east-2')
sg = aws_ec2_con.describe_security_groups()
sg_id = sg.get('SecurityGroups', [{}])[1].get('GroupId')
print(sg_id)
# sg-008a29361b496b632
# sg-0c5ce097f6a6361e2
# Delete security group
try:
    response = aws_ec2_con.delete_security_group(GroupId=sg_id)
    print('Security Group Deleted %s' %sg_id)
except ClientError as e:
    print(e)