import boto3
from botocore.client import ClientError

aws_man_con = boto3.session.Session(profile_name='amit')
aws_ec2_con = aws_man_con.client('ec2', 'us-east-2')

response = aws_ec2_con.describe_vpcs()
vpc_id = response.get('Vpcs', [{}])[0].get('VpcId','')
# print(response)
# print(vpc_id)
try:
    response = aws_ec2_con.create_security_group(GroupName='sg_demo',
                                         Description='sg_demo',
                                         VpcId=vpc_id)
    security_group_id = response['GroupId']
    print('Security Group Created %s in vpc %s.' % (security_group_id, vpc_id))

    data = aws_ec2_con.authorize_security_group_ingress(
        GroupId=security_group_id,
        IpPermissions=[
            {'IpProtocol': 'tcp',
             'FromPort': 80,
             'ToPort': 80,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
            {'IpProtocol': 'tcp',
             'FromPort': 22,
             'ToPort': 22,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}
        ])
    print('Ingress Successfully Set %s' % data)
except ClientError as e:
    print(e)

