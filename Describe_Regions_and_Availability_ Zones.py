import boto3


aws_man_con = boto3.session.Session(profile_name='amit')
aws_ec2_con = aws_man_con.client('ec2')
# ec2 = boto3.client('ec2')

# Retrieves all regions/endpoints that work with EC2
response = aws_ec2_con.describe_regions()
print('Regions:', response['Regions'])

# Retrieves availability zones only for region of the ec2 object
response = aws_ec2_con.describe_availability_zones()
print('Availability Zones:', response['AvailabilityZones'])